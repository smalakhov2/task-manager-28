#!/usr/bin/env bash

echo "RESTART SERVER...";

if [ ! -f ./server.pid ]; then
  echo "server.pid not found!"
  exit 1;
fi

./shutdown.sh
./startup.sh