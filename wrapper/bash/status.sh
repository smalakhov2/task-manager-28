#!/usr/bin/env bash

echo "SERVER STATUS..."

if [ ! -f ./server.pid ]; then
  echo "SERVER STOPPED."
  exit 1;
fi

echo "SERVER STARTED."
read -p "Do you want see server log?(y/n):" response

if [ $response = "y" ]; then
  cat ./log/server.log
fi
