package ru.malakhov.tm.repository;

import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.api.repository.IPropertyRepository;
import ru.malakhov.tm.endpoint.SessionDto;

public final class PropertyRepository implements IPropertyRepository {

    @Nullable
    private SessionDto session;

    @Nullable
    @Override
    public SessionDto getSession() {
        return session;
    }

    @Override
    public void setSession(@Nullable final SessionDto session) {
        this.session = session;
    }

}