package ru.malakhov.tm;

import org.jetbrains.annotations.NotNull;
import ru.malakhov.tm.bootstrap.Bootstrap;

public final class Application {

    public static void main(@NotNull final String[] args) {
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        bootstrap.run(args);
    }

}