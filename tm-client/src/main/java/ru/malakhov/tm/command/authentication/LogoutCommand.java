package ru.malakhov.tm.command.authentication;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.endpoint.Result;
import ru.malakhov.tm.endpoint.SessionDto;

public final class LogoutCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "logout";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Logout.";
    }

    @Override
    public void execute() {
        System.out.println("[LOGOUT]");
        @Nullable final SessionDto session = serviceLocator.getPropertyService().getSession();
        @NotNull final Result result = serviceLocator.getSessionEndpoint().closeSession(session);
        if (result.isSuccess()) {
            System.out.println("[OK]");
            serviceLocator.getPropertyService().setSession(null);
        } else System.out.println("[FAIL]");
    }

    @Override
    public boolean secure() {
        return true;
    }

}