package ru.malakhov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.endpoint.ProjectDto;
import ru.malakhov.tm.endpoint.SessionDto;
import ru.malakhov.tm.util.TerminalUtil;

public final class ProjectShowByIndexCommand extends AbstractProjectShowCommand {

    @NotNull
    @Override
    public String name() {
        return "project-show-by-index";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "project-show-by-index";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[SHOW PROJECT]");
        System.out.print("ENTER INDEX: ");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @Nullable final SessionDto session = serviceLocator.getPropertyService().getSession();
        @Nullable final ProjectDto project = serviceLocator.getProjectEndpoint().getProjectByIndex(session, index);
        if (project == null) System.out.println("[FAIL]");
        showProject(project);
        System.out.println("[OK]");
    }

    @Override
    public boolean secure() {
        return true;
    }

}