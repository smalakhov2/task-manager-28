package ru.malakhov.tm.command.admin.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.endpoint.AbstractException_Exception;
import ru.malakhov.tm.endpoint.Result;
import ru.malakhov.tm.endpoint.SessionDto;

public final class DataJsonSaveCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "data-json-save";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Extracting application data in JSON format.";
    }

    @Override
    public void execute() throws AbstractException_Exception {
        System.out.println("[DATA JSON SAVE]");
        @Nullable final SessionDto session = serviceLocator.getPropertyService().getSession();
        @NotNull final Result result = serviceLocator.getAdminDataEndpoint().saveDataJson(session);
        if (result.isSuccess()) System.out.println("[OK]");
        else {
            System.out.println("MESSAGE" + result.getMessage());
            System.out.println("[FAIL]");
        }
    }

    @Override
    public boolean secure() {
        return true;
    }

}