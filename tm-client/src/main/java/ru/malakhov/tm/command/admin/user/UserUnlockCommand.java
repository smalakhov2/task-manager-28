package ru.malakhov.tm.command.admin.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.endpoint.AbstractException_Exception;
import ru.malakhov.tm.endpoint.Result;
import ru.malakhov.tm.endpoint.SessionDto;
import ru.malakhov.tm.util.TerminalUtil;

public class UserUnlockCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "user-unlock";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Unlock user account by login.";
    }

    @Override
    public void execute() throws AbstractException_Exception {
        System.out.println("[UNLOCK-USER]");
        System.out.print("ENTER USER LOGIN:");
        @NotNull final String login = TerminalUtil.nextLine();
        @Nullable final SessionDto session = serviceLocator.getPropertyService().getSession();
        @NotNull final Result result = serviceLocator.getAdminUserEndpoint().unlockUserByLogin(session, login);
        System.out.println("[OK]");
        if (result.isSuccess()) System.out.println("[OK]");
        else {
            System.out.println("MESSAGE" + result.getMessage());
            System.out.println("[FAIL]");
        }
    }

    @Override
    public boolean secure() {
        return true;
    }

}
