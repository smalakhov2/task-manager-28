package ru.malakhov.tm.util;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import ru.malakhov.tm.exception.system.IndexIncorrectException;

import java.util.Scanner;

@UtilityClass
public final class TerminalUtil {

    @NotNull
    private static final Scanner SCANNER = new Scanner(System.in);

    @NotNull
    public static String nextLine() {
        return SCANNER.nextLine();
    }

    @NotNull
    public static Integer nextNumber() throws IndexIncorrectException {
        final String value = nextLine();
        try {
            return Integer.parseInt(value);
        } catch (Exception e) {
            throw new IndexIncorrectException(value, e);
        }
    }

}