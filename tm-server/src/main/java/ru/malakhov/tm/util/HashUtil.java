package ru.malakhov.tm.util;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@UtilityClass
public final class HashUtil {

    @NotNull
    private static final String SECRET = "12124432235";

    @NotNull
    private static final Integer ITERATION = 327;

    @Nullable
    public static String salt(@Nullable final String value) {
        if (value == null || value.isEmpty()) return null;
        @Nullable String result = value;
        for (int i = 0; i < ITERATION; i++) {
            if (result != null) result = md5(SECRET + value + SECRET);
        }
        return result;
    }

    @Nullable
    public static String md5(@Nullable final String value) {
        if (value == null || value.isEmpty()) return null;
        try {
            @NotNull final java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            @NotNull final byte[] array = md.digest(value.getBytes());
            @NotNull final StringBuffer stringBuffer = new StringBuffer();
            for (int i = 0; i < array.length; ++i) {
                stringBuffer.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1, 3));
            }
            return stringBuffer.toString();
        } catch (java.security.NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }

}
