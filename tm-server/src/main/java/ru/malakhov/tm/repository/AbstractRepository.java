package ru.malakhov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.api.repository.IRepository;
import ru.malakhov.tm.dto.AbstractEntityDto;

import javax.persistence.EntityManager;
import java.util.Collection;

public abstract class AbstractRepository<E extends AbstractEntityDto> implements IRepository<E> {

    @NotNull
    protected final EntityManager em;

    public AbstractRepository(@NotNull final EntityManager em) {
        this.em = em;
    }

    @Override
    public void begin(){
        em.getTransaction().begin();
    }

    @Override
    public void commit(){
        em.getTransaction().commit();
    }

    @Override
    public void rollback(){
        em.getTransaction().rollback();
    }

    @Override
    public void close(){
        em.close();
    }

    @Override
    public void clear(){
        em.clear();
    }

    @Override
    public void persist(@NotNull final E entity) {
        em.persist(entity);
    }

    @Override
    public void persist(@NotNull final Collection<E> entities) {
        for (@NotNull final E entity: entities) {
            em.persist(entity);
        }
    }

    @Override
    @SafeVarargs
    public final void persist(@Nullable final E... entities) {
        for (E entity : entities) if (entity != null) em.persist(entity);
    }

    @Override
    public void merge(@NotNull final E entity) {
        em.merge(entity);
    }

    @Override
    public void merge(@NotNull final Collection<E> entities) {
        for (@NotNull final E entity: entities) {
            em.merge(entity);
        }
    }

    @Override
    @SafeVarargs
    public final void merge(@Nullable final E... entities) {
        for (E entity : entities) if (entity != null) em.merge(entity);
    }

    @Override
    public void removeOne(@NotNull E entity) {
        em.remove(entity);
    }

}
