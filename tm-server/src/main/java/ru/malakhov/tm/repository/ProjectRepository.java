package ru.malakhov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.api.repository.IProjectRepository;
import ru.malakhov.tm.dto.ProjectDto;
import ru.malakhov.tm.entity.Project;

import javax.persistence.EntityManager;
import java.util.List;

public final class ProjectRepository extends AbstractRepository<ProjectDto> implements IProjectRepository {

    public ProjectRepository(@NotNull final EntityManager em) {
        super(em);
    }

    @NotNull
    @Override
    public List<ProjectDto> findAllDto() {
        return em.createQuery("SELECT e FROM ProjectDto e", ProjectDto.class).getResultList();
    }

    @NotNull
    @Override
    public List<Project> findAllEntity() {
        return em.createQuery("SELECT e FROM Project e", Project.class).getResultList();
    }

    @NotNull
    @Override
    public List<ProjectDto> findAllDtoByUserId(@NotNull final String userId) {
        @NotNull final List<ProjectDto> projects =
                em.createQuery("SELECT e FROM ProjectDto e WHERE e.userId=:userId", ProjectDto.class)
                        .setParameter("userId", userId)
                        .getResultList();
        return projects;
    }

    @NotNull
    @Override
    public List<Project> findAllEntityByUserId(@NotNull final String userId) {
        @NotNull final List<Project> projects =
                em.createQuery("SELECT e FROM Project e WHERE e.user.id=:userId", Project.class)
                        .setParameter("userId", userId)
                        .getResultList();
        return projects;
    }

    @Nullable
    @Override
    public ProjectDto findOneDtoById(@NotNull final String id) {
        @NotNull final List<ProjectDto> projects =
                em.createQuery("SELECT e FROM ProjectDto e WHERE e.id=:id", ProjectDto.class)
                        .setParameter("id", id)
                        .setMaxResults(1)
                        .getResultList();
        if (projects.isEmpty()) return null;
        return projects.get(0);
    }

    @Nullable
    @Override
    public Project findOneEntityById(@NotNull final String id) {
        @NotNull final List<Project> projects =
                em.createQuery("SELECT e FROM Project e WHERE e.id=:id", Project.class)
                        .setParameter("id", id)
                        .setMaxResults(1)
                        .getResultList();
        if (projects.isEmpty()) return null;
        return projects.get(0);
    }

    @Nullable
    @Override
    public ProjectDto findOneDtoById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final List<ProjectDto> projects =
                em.createQuery("SELECT e FROM ProjectDto e WHERE e.userId=:userId AND e.id=:id", ProjectDto.class)
                        .setParameter("userId", userId)
                        .setParameter("id", id)
                        .setMaxResults(1)
                        .getResultList();
        if (projects.isEmpty()) return null;
        return projects.get(0);
    }

    @Nullable
    @Override
    public Project findOneEntityById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final List<Project> projects =
                em.createQuery("SELECT e FROM Project e WHERE e.user.id=:userId AND e.id=:id", Project.class)
                        .setParameter("userId", userId)
                        .setParameter("id", id)
                        .setMaxResults(1)
                        .getResultList();
        if (projects.isEmpty()) return null;
        return projects.get(0);
    }

    @Nullable
    @Override
    public ProjectDto findOneDtoByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @NotNull final List<ProjectDto> projects =
                em.createQuery("SELECT e FROM ProjectDto e WHERE e.userId=:userId ORDER BY e.name ASC", ProjectDto.class)
                        .setParameter("userId", userId)
                        .getResultList();
        if (index >= projects.size()) return null;
        return projects.get(index);
    }

    @Nullable
    @Override
    public Project findOneEntityByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @NotNull final List<Project> projects =
                em.createQuery("SELECT e FROM Project e WHERE e.user.id=:userId ORDER BY e.name ASC", Project.class)
                        .setParameter("userId", userId)
                        .getResultList();
        if (index >= projects.size()) return null;
        return projects.get(index);
    }

    @Nullable
    @Override
    public ProjectDto findOneDtoByName(@NotNull final String userId, @NotNull final String name) {
        @NotNull final List<ProjectDto> projects =
                em.createQuery("SELECT e FROM ProjectDto e WHERE e.userId=:userId AND e.name=:name", ProjectDto.class)
                        .setParameter("userId", userId)
                        .setParameter("name", name)
                        .setMaxResults(1)
                        .getResultList();
        if (projects.isEmpty()) return null;
        return projects.get(0);
    }

    @Nullable
    @Override
    public Project findOneEntityByName(@NotNull final String userId, @NotNull final String name) {
        @NotNull final List<Project> projects =
                em.createQuery("SELECT e FROM Project e WHERE e.user.id=:userId AND e.name=:name", Project.class)
                        .setParameter("userId", userId)
                        .setParameter("name", name)
                        .setMaxResults(1)
                        .getResultList();
        if (projects.isEmpty()) return null;
        return projects.get(0);
    }

    @Override
    public void removeAll() {
        @NotNull final List<Project> projects = findAllEntity();
        for (@NotNull final Project project: projects) em.remove(project);
    }

    @Override
    public void removeAllByUserId(@NotNull final String userId) {
        @NotNull final List<Project> projects = findAllEntityByUserId(userId);
        for (@NotNull final Project project: projects) em.remove(project);
    }

    @Override
    public void removeOneById(@NotNull final String id) {
        @Nullable final Project project = findOneEntityById(id);
        if (project == null) return;
        em.remove(project);
    }

    @Override
    public void removeOneById(@NotNull final String userId, @NotNull final String id) {
        @Nullable final Project project = findOneEntityById(userId, id);
        if (project == null) return;
        em.remove(project);
    }

    @Override
    public void removeOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @Nullable final Project project = findOneEntityByIndex(userId, index);
        if (project == null) return;
        em.remove(project);
    }

    @Override
    public void removeOneByName(@NotNull final String userId, @NotNull final String name) {
        @Nullable final Project project = findOneEntityByName(userId, name);
        if (project == null) return;
        em.remove(project);
    }

}
