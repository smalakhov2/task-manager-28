package ru.malakhov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.api.repository.ITaskRepository;
import ru.malakhov.tm.dto.TaskDto;
import ru.malakhov.tm.entity.Task;
import ru.malakhov.tm.exception.AbstractException;
import ru.malakhov.tm.exception.empty.EmptyIdException;
import ru.malakhov.tm.exception.empty.EmptyUserIdException;

import java.util.List;

public interface ITaskService extends IService<TaskDto, ITaskRepository> {

    void create(@Nullable String userId, @Nullable String name) throws AbstractException;

    void create(@Nullable String userId, @Nullable String name, @Nullable String description) throws AbstractException;

    @NotNull
    List<TaskDto> findAllDto();

    @NotNull
    List<Task> findAllEntity();

    @NotNull
    List<TaskDto> findAllDtoByUserId(@Nullable String userId) throws EmptyUserIdException;

    @NotNull
    List<Task> findAllEntityByUserId(@Nullable String userId) throws EmptyUserIdException;

    @Nullable
    TaskDto findOneDtoById(@Nullable String id) throws EmptyIdException;

    @Nullable
    Task findOneEntityById(@Nullable String id) throws EmptyIdException;

    @Nullable
    TaskDto findOneDtoById(@Nullable String userId, @Nullable String id) throws AbstractException;

    @Nullable
    Task findOneEntityById(@Nullable String userId, @Nullable String id) throws AbstractException;

    @Nullable
    TaskDto findOneDtoByIndex(@Nullable String userId, @Nullable Integer index) throws AbstractException;

    @Nullable
    Task findOneEntityByIndex(@Nullable String userId, @Nullable Integer index) throws AbstractException;

    @Nullable
    TaskDto findOneDtoByName(@Nullable String userId, @Nullable String name) throws AbstractException;

    @Nullable
    Task findOneEntityByName(@Nullable String userId, @Nullable String name) throws AbstractException;

    void removeAll();

    void removeAllByUserId(@Nullable String userId) throws EmptyUserIdException;

    void removeOneById(@Nullable String id) throws EmptyIdException;

    void removeOneById(@Nullable String userId, @Nullable String id) throws AbstractException;

    void removeOneByIndex(@Nullable String userId, @Nullable Integer index) throws AbstractException;

    void removeOneByName(@Nullable String userId, @Nullable String name) throws AbstractException;

    void updateTaskById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    ) throws AbstractException;

    void updateTaskByIndex(
            @Nullable String userId,
            @Nullable Integer index,
            @Nullable String name,
            @Nullable String description
    ) throws AbstractException;

}