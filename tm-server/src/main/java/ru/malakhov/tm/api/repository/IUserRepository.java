package ru.malakhov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.dto.UserDto;
import ru.malakhov.tm.entity.User;

import java.util.List;

public interface IUserRepository extends IRepository<UserDto> {

    @NotNull
    List<UserDto> findAllDto();

    @NotNull
    List<User> findAllEntity();

    @Nullable
    UserDto findOneDtoById(@NotNull String id);

    @Nullable
    User findOneEntityById(@NotNull String id);

    @Nullable
    UserDto findOneDtoByLogin(@NotNull String login);

    @Nullable
    User findOneEntityByLogin(@NotNull String login);

    void removeAll();

    void removeOneById(@NotNull String id);

    void removeOneByLogin(@NotNull String login);

}