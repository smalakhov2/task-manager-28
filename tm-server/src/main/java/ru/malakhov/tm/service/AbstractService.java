package ru.malakhov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.api.ISqlSessionProvider;
import ru.malakhov.tm.api.repository.IRepository;
import ru.malakhov.tm.api.service.IService;
import ru.malakhov.tm.dto.AbstractEntityDto;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.Objects;

public abstract class AbstractService<E extends AbstractEntityDto, R extends IRepository<E>> implements IService<E, R> {

    @NotNull
    protected final ISqlSessionProvider sqlSessionProvider;

    public AbstractService(@NotNull final ISqlSessionProvider sqlSessionProvider) {
        this.sqlSessionProvider = sqlSessionProvider;
    }

    @NotNull
    @Override
    public EntityManager getEntityManager() {
        return sqlSessionProvider.getEntityManager();
    }

    @Override
    public void persist(@Nullable final E entity) {
        if (entity == null) return;
        @NotNull final R repository = getRepository();

        try {
            repository.begin();
            repository.persist(entity);
            repository.commit();
        } catch (Exception e) {
            repository.rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @Override
    public void persist(@Nullable final Collection<E> entities) {
        if (entities == null) return;
        entities.removeIf(Objects::isNull);
        @NotNull final R repository = getRepository();

        try {
            repository.begin();
            repository.persist(entities);
            repository.commit();
        } catch (Exception e) {
            repository.rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @Override
    @SafeVarargs
    public final void persist(@Nullable final E... entities) {
        if (entities == null) return;
        @NotNull final R repository = getRepository();

        try {
            repository.begin();
            repository.persist(entities);
            repository.commit();
        } catch (Exception e) {
            repository.rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @Override
    public void merge(@Nullable final E entity) {
        if (entity == null) return;
        @NotNull final R repository = getRepository();

        try {
            repository.begin();
            repository.merge(entity);
            repository.commit();
        } catch (Exception e) {
            repository.rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @Override
    public void merge(@Nullable final Collection<E> entities) {
        if (entities == null) return;
        entities.removeIf(Objects::isNull);
        @NotNull final R repository = getRepository();

        try {
            repository.begin();
            repository.merge(entities);
            repository.commit();
        } catch (Exception e) {
            repository.rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @Override
    @SafeVarargs
    public final void merge(@Nullable final E... entities) {
        if (entities == null) return;
        @NotNull final R repository = getRepository();

        try {
            repository.begin();
            repository.merge(entities);
            repository.commit();
        } catch (Exception e) {
            repository.rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @Override
    public void removeOne(@Nullable E entity) {
        if (entity == null) return;
        @NotNull final R repository = getRepository();

        try {
            repository.begin();
            repository.removeOne(entity);
            repository.commit();
        } catch (Exception e) {
            repository.rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @NotNull
    @Override
    public abstract R getRepository();

}
