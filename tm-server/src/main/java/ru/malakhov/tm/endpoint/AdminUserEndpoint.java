package ru.malakhov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.api.endpoint.IAdminUserEndpoint;
import ru.malakhov.tm.api.service.IServiceLocator;
import ru.malakhov.tm.dto.SessionDto;
import ru.malakhov.tm.dto.UserDto;
import ru.malakhov.tm.dto.response.Fail;
import ru.malakhov.tm.dto.response.Result;
import ru.malakhov.tm.dto.response.Success;
import ru.malakhov.tm.enumerated.Role;
import ru.malakhov.tm.exception.AbstractException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public class AdminUserEndpoint extends AbstractEndpoint implements IAdminUserEndpoint {

    public AdminUserEndpoint() {
        super(null);
    }

    public AdminUserEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    @Override
    @WebMethod
    public Result clearAllUser(
            @WebParam(name = "session") @Nullable final SessionDto session
    ) throws AbstractException {
        try {
            serviceLocator.getSessionService().validate(session, Role.ADMIN);
            serviceLocator.getUserService().removeAll();
            return new Success();
        } catch (@NotNull final AbstractException e) {
            return new Fail(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public List<UserDto> getAllUserList(
            @WebParam(name = "session") @Nullable final SessionDto session
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().findAllDto();
    }

    @NotNull
    @Override
    @WebMethod
    public Result lockUserByLogin(
            @WebParam(name = "session") @Nullable final SessionDto session,
            @WebParam(name = "login") @Nullable final String login
    ) {
        try {
            serviceLocator.getSessionService().validate(session, Role.ADMIN);
            serviceLocator.getSessionService().signOutByLogin(login);
            serviceLocator.getUserService().lockUserByLogin(login);
            return new Success();
        } catch (@NotNull final AbstractException e) {
            return new Fail(e);
        }


    }

    @NotNull
    @Override
    @WebMethod
    public Result unlockUserByLogin(
            @WebParam(name = "session") @Nullable final SessionDto session,
            @WebParam(name = "login") @Nullable final String login
    ) {
        try {
            serviceLocator.getSessionService().validate(session, Role.ADMIN);
            serviceLocator.getUserService().unlockUserByLogin(login);
            return new Success();
        } catch (@NotNull final AbstractException e) {
            return new Fail(e);
        }
    }

    @Nullable
    @Override
    @WebMethod
    public Result removeUserByLogin(
            @WebParam(name = "session") @Nullable final SessionDto session,
            @WebParam(name = "login") @Nullable final String login
    ) {
        try {
            serviceLocator.getSessionService().validate(session, Role.ADMIN);
            serviceLocator.getSessionService().signOutByLogin(login);
            serviceLocator.getUserService().removeOneByLogin(login);
            return new Success();
        } catch (@NotNull final AbstractException e) {
            return new Fail(e);
        }
    }

}