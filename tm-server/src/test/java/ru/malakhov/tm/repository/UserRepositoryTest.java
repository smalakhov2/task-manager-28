package ru.malakhov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.malakhov.tm.AbstractDataTest;
import ru.malakhov.tm.api.repository.IUserRepository;
import ru.malakhov.tm.category.DataCategory;
import ru.malakhov.tm.dto.UserDto;
import ru.malakhov.tm.entity.User;
import ru.malakhov.tm.enumerated.Role;
import ru.malakhov.tm.exception.AbstractException;

import java.util.List;

@Category(DataCategory.class)
public final class UserRepositoryTest extends AbstractDataTest {

    @NotNull
    private IUserRepository getRepository() {
        return bootstrap.getUserService().getRepository();
    }
    
    public UserRepositoryTest() throws Exception {
        super();
    }

    @Before
    public void before() {
        bootstrap.getUserService().persist(userDto, adminDto);
    }

    @After
    public void after() {
        bootstrap.getUserService().removeOne(userDto);
        bootstrap.getUserService().removeOne(adminDto);
    }

    @Test
    public void testFindAllDto() {
        @NotNull final IUserRepository repository = getRepository();
        @NotNull final List<UserDto> users = repository.findAllDto();
        Assert.assertEquals(4, users.size());
    }

    @Test
    public void testFindAllEntity() {
        @NotNull final IUserRepository repository = getRepository();
        @NotNull final List<User> users = repository.findAllEntity();
        Assert.assertEquals(4, users.size());
    }

    @Test
    public void testFindOneDtoById() {
        @NotNull final IUserRepository repository = getRepository();

        @Nullable final UserDto user = repository.findOneDtoById(userDto.getId());
        Assert.assertNotNull(user);
        Assert.assertEquals(user, userDto);

        @Nullable final UserDto unknown = repository.findOneDtoById(unknownUserDto.getId());
        Assert.assertNull(unknown);
    }

    @Test
    public void testFindOneEntityById() {
        @NotNull final IUserRepository repository = getRepository();

        @Nullable final User user = repository.findOneEntityById(userDto.getId());
        Assert.assertNotNull(user);

        @Nullable final User unknown = repository.findOneEntityById(unknownUserDto.getId());
        Assert.assertNull(unknown);
    }

    @Test
    public void testFindOneDtoByLogin() {
        @NotNull final IUserRepository repository = getRepository();

        @Nullable final UserDto user = repository.findOneDtoByLogin(userDto.getLogin());
        Assert.assertNotNull(user);
        Assert.assertEquals(user, userDto);

        @Nullable final UserDto unknown = repository.findOneDtoByLogin(unknownUserDto.getLogin());
        Assert.assertNull(unknown);
    }

    @Test
    public void testFindOneEntityByLogin() {
        @NotNull final IUserRepository repository = getRepository();

        @Nullable final User user = repository.findOneEntityByLogin(userDto.getLogin());
        Assert.assertNotNull(user);

        @Nullable final User unknown = repository.findOneEntityByLogin(unknownUserDto.getLogin());
        Assert.assertNull(unknown);
    }

    @Test
    public void testRemoveAll() throws AbstractException {
        @NotNull final IUserRepository repository = getRepository();
        try {
            repository.begin();
            repository.removeAll();
            repository.commit();
            @NotNull final List<UserDto> users = repository.findAllDto();
            Assert.assertEquals(0, users.size());
        } catch (Exception e) {
            repository.rollback();
            throw e;
        } finally {
            repository.close();
        }

        bootstrap.getUserService().create("admin", "admin", Role.ADMIN);
        bootstrap.getUserService().create("test", "test");
    }

    @Test
    public void testRemoveOneById() {
        @NotNull final IUserRepository repository = getRepository();
        try {
            repository.begin();
            repository.removeOneById(unknownUserDto.getId());
            repository.commit();
            @NotNull List<UserDto> users = repository.findAllDto();
            Assert.assertEquals(4, users.size());

            repository.begin();
            repository.removeOneById(userDto.getId());
            repository.commit();
            @Nullable final UserDto user = repository.findOneDtoById(userDto.getId());
            Assert.assertNull(user);
        } catch (Exception e) {
            repository.rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @Test
    public void testRemoveOneByLogin() {
        @NotNull final IUserRepository repository = getRepository();
        try {
            repository.begin();
            repository.removeOneByLogin(unknownUserDto.getLogin());
            repository.commit();
            @NotNull List<UserDto> users = repository.findAllDto();
            Assert.assertEquals(4, users.size());

            repository.begin();
            repository.removeOneByLogin(userDto.getLogin());
            repository.commit();
            @Nullable final UserDto user = repository.findOneDtoById(userDto.getId());
            Assert.assertNull(user);
        } catch (Exception e) {
            repository.rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

}
